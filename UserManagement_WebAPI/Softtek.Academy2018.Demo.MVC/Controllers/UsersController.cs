﻿using Softtek.Academy2018.Demo.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace Softtek.Academy2018.Demo.MVC.Controllers
{
    public class UsersController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            //List<User> users = new List<User>();
            //User user = new User { Id = 1, IS = "ALNH", FirstName = "Alan", LastName = "Navarro", ProjectId = 1, Project = new Project { Id = 1, Name = "Academia", TechnologyStack = ".NET"}, Salary = 1 };
            //users.Add(user);
            //user = new User { Id = 2, IS = "PEPI", FirstName = "Pepito", LastName = "Pepito", ProjectId = 1, Project = new Project { Id = 1, Name = "Academia", TechnologyStack = ".NET" }, Salary = 1 };
            //users.Add(user);

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:52432/api/user");
                //HTTP GET
                var responseTask = client.GetAsync("User");
                responseTask.Wait();
                var result = responseTask.Result;
                var readTask = result.Content.ReadAsAsync<Users[]>();
                readTask.Wait();
                result = responseTask.Result;
                return View(result);
            }
        }
    }
}