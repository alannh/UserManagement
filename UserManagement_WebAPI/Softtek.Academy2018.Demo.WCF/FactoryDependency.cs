﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Softtek.Academy2018.Demo.WCF
{
    public class FactoryDependency
    {
        public static IKernel GetKernel()
        {
            StandardKernel kernel = new StandardKernel();

            //Bindings
            kernel.Bind<IUserRepository>().To<UserDataRepository>();
            //kernel.Bind<IUserRepository>().To<UserFakeRepository>();
            kernel.Bind<IUserService>().To<UserService>();
            kernel.Bind<IProjectRepository>().To<ProjectDataRepository>();
            kernel.Bind<IProjectService>().To<ProjectService>();

            return kernel;
        }
    }
}