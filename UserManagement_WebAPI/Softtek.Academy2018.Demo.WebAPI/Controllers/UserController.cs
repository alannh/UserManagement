﻿using Softtek.Academy2018.Demo.Business.Contracts;
using Softtek.Academy2018.Demo.Business.Implementation;
using Softtek.Academy2018.Demo.Data.Contracts;
using Softtek.Academy2018.Demo.Data.Implementation;
using Softtek.Academy2018.Demo.Domain.Model;
using Softtek.Academy2018.Demo.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Softtek.Academy2018.Demo.WebAPI.Controllers
{
    [RoutePrefix("api/user")]
    public class UserController : ApiController
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult Create([FromBody] UserDTO userDTO)
        {
            if (userDTO == null) return BadRequest("Request is Null");

            User user = new User
            {
                IS = userDTO.IS,
                FirstName = userDTO.FirstName,
                LastName = userDTO.LastName,
                DateOfBirth = userDTO.DateOfBirth,
                Salary = userDTO.Salary,
                CreateDate = userDTO.CreateDate,
                ModifyDate = userDTO.ModifyDate
            };

            int id = _userService.Add(user);

            if (id <= 0) return BadRequest("Unable to create user");

            var payload = new { UserId = id };

            return Ok(payload);

        }

        [Route("{id:int}")]
        [HttpGet]
        public IHttpActionResult Get([FromUri] int id)
        {
            User user = _userService.Get(id);

            if (user == null) return NotFound();

            UserDTO userDTO = new UserDTO()
            {
                Id = user.Id,
                IS = user.IS,
                FirstName = user.FirstName,
                LastName = user.LastName,
                DateOfBirth = user.DateOfBirth,
                Salary = user.Salary,
                CreateDate = user.CreatedDate,
                ModifyDate = user.ModifiedDate
            };

            return Ok(user);
        }


        [Route("{id:int}")]
        [HttpDelete]
        public IHttpActionResult Delete([FromUri] int id)
        {
            var result = _userService.Delete(id);

            if (!result) return BadRequest("Unable to delete user");

            return Ok();
        }

        [Route("{int:id}")]
        [HttpPut]
        public IHttpActionResult Update([FromUri]int id, [FromBody] UserDTO userDTO)
        {
            if (userDTO == null) return BadRequest("Null user");

            var result = _userService.Get(userDTO.Id);

            if (result == null) return BadRequest("Invalid User ID");

            User user = new User
            {
                Id = id,
                FirstName = userDTO.FirstName,
                LastName = userDTO.LastName,
                Salary = userDTO.Salary,
                IS = userDTO.IS,
                CreateDate = userDTO.CreateDate,
                ModifyDate = DateTime.Now,
                DateOfBirth=userDTO.DateOfBirth
            };

            bool userUpdated = _userService.Update(user);

            if (!userUpdated) return BadRequest("Unable to update user");

            return Ok();
        }
    }
}