﻿using Softtek.Academy2018.Demo.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.Demo.Client.Contract
{
    public interface IUserClient
    {
        //Standard Methods
        Task<UserDTO> Get(int id);

        Task<ICollection<UserDTO>> GetAll();

        Task<bool> Create(UserDTO user);

        Task<bool> Update(UserDTO user);

        Task<bool> Delete(int id);

    }
}
