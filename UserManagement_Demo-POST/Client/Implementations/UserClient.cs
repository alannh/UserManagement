﻿using Softtek.Academy2018.Demo.Client.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using Softtek.Academy2018.Demo.DTO;

namespace Softtek.Academy2018.Demo.Client.Implementation
{
    public class UserClient : IUserClient
    {
        private readonly Uri baseUrl;
        private readonly MediaTypeWithQualityHeaderValue mediaType;

        public UserClient()
        {
            baseUrl = new Uri("http://localhost:2311/");
            mediaType = new MediaTypeWithQualityHeaderValue("application/json");
        }

        //string BaseUrl = "http://localhost:2311/";

        //Methods declarados como async

        public async Task<bool> Create(UserDTO user)    //bool return
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = baseUrl;
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(mediaType); //application/json
                //var response = await client.PostAsJsonAsync($"api/user", user);
                var response = await client.PostAsJsonAsync("user/new", user);

                bool success = response.IsSuccessStatusCode;
                //if (success) return RedirectToAction("Index", "Users");
                //return RedirectToAction("NewFail", "Users");

                //return response.IsSuccessStatusCode;
                return success;
            }
        }

        public async Task<bool> Delete(int id)
        {
            using (var client = new HttpClient())
            {
                //var response = await client.GetAsync($"api/user/{id}");

                var response = await client.GetAsync($"user/delete/{id}");

                string content = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return false;
                }

                var userDB = JsonConvert.DeserializeObject<UserDTO>(content);

                response = await client.DeleteAsync($"user/delete/{id}");

                return response.IsSuccessStatusCode;

            }
        }

        public async Task<UserDTO> Get(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = baseUrl;

                client.DefaultRequestHeaders.Clear();

                client.DefaultRequestHeaders.Accept.Add(mediaType);

                var response = await client.GetAsync($"api/user/{id}");

                string content = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return null;
                }

                UserDTO user = JsonConvert.DeserializeObject<UserDTO>(content);

                return user;
            }
        }

        public async Task<ICollection<UserDTO>> GetAll()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = baseUrl;
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(mediaType);

                var response = await client.GetAsync("user/index/");    //Check this endpoint

                string content = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return null;
                }

                //List<UserViewModel> users = JsonConvert.DeserializeObject<List<UserViewModel>>(content);

                var users = JsonConvert.DeserializeObject<List<UserDTO>>(content);

                return users;
            }
        }

        public async Task<bool> Update(UserDTO user)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = baseUrl;
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(mediaType);

                //var response = await client.GetAsync($"api/user/{user.Id}");
                var response = await client.GetAsync($"user/update/{user.Id}");

                string content = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return false;
                }

                var userDB = JsonConvert.DeserializeObject<UserDTO>(content);

                userDB.Id = user.Id;
                userDB.IS = user.IS;
                userDB.FirstName = user.FirstName;
                userDB.LastName = user.LastName;
                userDB.DateOfBirth = user.DateOfBirth;
                userDB.Salary = user.Salary;

                response = await client.PutAsJsonAsync($"user/update/{user.Id}", userDB);

                return response.IsSuccessStatusCode;

            }
        }
    }
}
