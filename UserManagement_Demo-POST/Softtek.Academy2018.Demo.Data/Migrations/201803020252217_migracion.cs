namespace Softtek.Academy2018.Demo.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migracion : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Projects",
                c => new
                {
                    ProjectId = c.Int(nullable: false, identity: true),
                    Name = c.String(),
                    Area = c.String(),
                    TechnologyStack = c.String(),
                })
                .PrimaryKey(t => t.ProjectId);

            CreateTable(
                "dbo.Users",
                c => new
                {
                    UserId = c.Int(nullable: false, identity: true),
                    IS = c.String(),
                    FirstName = c.String(),
                    LastName = c.String(),
                    DateOfBirth = c.DateTime(),
                    CreatedDate = c.DateTime(nullable: false),
                    ModifiedDate = c.DateTime(),
                    Salary = c.Double(nullable: false),
                    IsActive = c.Boolean(nullable: false),
                    ProjectId = c.Int(),
                })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Projects", t => t.ProjectId)
                .Index(t => t.ProjectId);

            DropForeignKey("dbo.Users", "ProjectId", "dbo.Projects");
            DropIndex("dbo.Users", new[] { "ProjectId" });
            AlterColumn("dbo.Users", "ProjectId", c => c.Int());
            CreateIndex("dbo.Users", "ProjectId");
            AddForeignKey("dbo.Users", "ProjectId", "dbo.Projects", "Id");

        }

        public override void Down()
        {
            DropForeignKey("dbo.Users", "ProjectId", "dbo.Projects");
            DropIndex("dbo.Users", new[] { "ProjectId" });
            DropTable("dbo.Users");
            DropTable("dbo.Projects");
        }
        //public override void Up()
        //{
        //    DropForeignKey("dbo.Users", "ProjectId", "dbo.Projects");
        //    DropIndex("dbo.Users", new[] { "ProjectId" });
        //    AlterColumn("dbo.Users", "ProjectId", c => c.Int());
        //    CreateIndex("dbo.Users", "ProjectId");
        //    AddForeignKey("dbo.Users", "ProjectId", "dbo.Projects", "Id");
        //}

        //public override void Down()
        //{
        //    DropForeignKey("dbo.Users", "ProjectId", "dbo.Projects");
        //    DropIndex("dbo.Users", new[] { "ProjectId" });
        //    AlterColumn("dbo.Users", "ProjectId", c => c.Int(nullable: false));
        //    CreateIndex("dbo.Users", "ProjectId");
        //    AddForeignKey("dbo.Users", "ProjectId", "dbo.Projects", "Id", cascadeDelete: true);
        //}
    }
}
