﻿using Softtek.Academy2018.Demo.Business.Contracts;
using Softtek.Academy2018.Demo.Domain.Model;
using Softtek.Academy2018.Demo.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Softtek.Academy2018.Demo.WebAPI.Controllers
{
    [RoutePrefix("api/project")]
    public class ProjectController : ApiController
    {
        private readonly IProjectService _projectService;

        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult Create([FromBody] ProjectDTO projectDTO)
        {
            if (projectDTO == null) return BadRequest("Request is null");

            Project project = new Project
            {
                Area = projectDTO.Area,
                Name = projectDTO.Name,
                TechnologyStack = projectDTO.TechnologyStack
            };

            int id = _projectService.Add(project);

            if (id <= 0) return BadRequest("Unable to create project");

            var payload = new { ProjectId = id };

            return Ok(payload);
        }

        [Route("~api/projects")]
        [HttpPost]
        public IHttpActionResult GetAll()
        {
            ICollection<Project> projects = _projectService.GetAll();

            return Ok(projects);
        }
    }
}