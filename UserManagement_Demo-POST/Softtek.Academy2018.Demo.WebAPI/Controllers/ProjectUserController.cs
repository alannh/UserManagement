﻿using Softtek.Academy2018.Demo.Business.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Softtek.Academy2018.Demo.WebAPI.Controllers
{
    public class ProjectUserController : ApiController
    {
        private readonly IProjectUserService _projectUserService;

        public ProjectUserController(IProjectUserService projectUserService)
        {
            _projectUserService = projectUserService;
        }

        [Route("api/project/{projectId:int}/user/{userid:int}")]
        [HttpPost]
        public IHttpActionResult AddUserProject([FromUri]int projectId, [FromUri]int userId)
        {
            bool isAdded = _projectUserService.AddUserToProject(projectId, userId);

            if(!isAdded) return BadRequest("Unable to asign user to project");

            return Ok("User added succesfully");
        }
    }
}