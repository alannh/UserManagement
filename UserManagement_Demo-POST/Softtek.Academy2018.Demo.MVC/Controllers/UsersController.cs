﻿using Newtonsoft.Json;
using Softtek.Academy2018.Demo.Client.Contract;
using Softtek.Academy2018.Demo.Client.Implementation;
using Softtek.Academy2018.Demo.Domain.Model;
using Softtek.Academy2018.Demo.DTO;
using Softtek.Academy2018.Demo.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Softtek.Academy2018.Demo.MVC.Controllers
{
    public class UsersController : Controller
    {
        IUserClient userClient = new UserClient();

        // GETALL Users
        public async Task<ActionResult> Index()
        {
            //GetAll method
            //using (var client = new HttpClient())
            //{
            //    client.BaseAddress = new Uri("http://localhost:2311/api/");
            //    var response = await client.GetAsync("user/index/");
            //    string content = await response.Content.ReadAsStringAsync();

            //    List<UserViewModel> users = JsonConvert.DeserializeObject<List<UserViewModel>>(content);

            //    return View(users);
            //}

            ICollection<UserDTO> users = new List<UserDTO>();

            users = await userClient.GetAll();

            //List<UserViewModel> usersViewModel = new UserViewModel
            //{
            //    FirstName = users
            //}

            return View(users); //Check if it complains without ViewModel

        }

        public ActionResult New()
        {
            return View();
        }


        //Aquí deben ir las rutas (?)
        [Route("users/details/{id}")]
        public async Task<ActionResult> Details(int id)
        {
            //*
            //ICollection<UserDTO> user = new List<UserDTO>();

            UserDTO user = new UserDTO();

            user = await userClient.Get(id); //Client Get method

            UserViewModel userViewModel = new UserViewModel
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                IS = user.IS
            };

            return View(userViewModel);
        }

        [Route("users/edit/{id}")]
        public async Task<ActionResult> Edit(int id)
        {
            var user = await userClient.Get(id);

            if (user == null)
            {
                return HttpNotFound();
            }

            return View(user);  //Check this view
        }


        [HttpPost]
        public async Task<ActionResult> Update(UserDTO user)
        {

            bool isUpdated = await userClient.Update(user);

            if (!isUpdated) return View("Error");

            return RedirectToAction("Index", "Users");

        }

        [HttpPost]  //Required
        public async Task<ActionResult> Create(UserDTO user)
        {
            if (!ModelState.IsValid)
            {
                return View("New");
            }

            bool isAdded = await userClient.Create(user);
            //if (!isAdded) return View("Error");
            if (isAdded) return RedirectToAction("Index", "Users");
            return RedirectToAction("NewFail", "Users");
            //return RedirectToAction("Index", "Users");
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {

            bool isDeleted = await userClient.Delete(id);

            if (!isDeleted) return View("Error");

            return RedirectToAction("Index", "Users");
        }
    }
}
