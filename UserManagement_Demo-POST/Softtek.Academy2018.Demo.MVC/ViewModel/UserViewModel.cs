﻿using Softtek.Academy2018.Demo.Domain.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web;

namespace Softtek.Academy2018.Demo.MVC.ViewModel
{
    public class UserViewModel
    {
        public int? Id { get; set; }

        public string IS { get; set; }
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Display(Name = "Date of Birth")]
        public DateTime? DateOfBirth { get; set; }
        [Display(Name = "Creation Date")]
        public DateTime? CreatedDate { get; set; }
        [Display(Name = "Modified Date")]
        public DateTime? ModifiedDate { get; set; }

        public Double Salary { get; set; }

        public bool IsActive { get; set; }

        public int? ProjectId { get; set; }

        public Project Project { get; set; }

        public string ActionParameter
        {
            get
            {
                var param = new StringBuilder(@"/");

                param.Append($"{Id}");

                return param.ToString();
            }

        }
    }
}