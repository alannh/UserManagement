﻿using Softtek.Academy2018.Demo.Console.ProjectManagementService_Ref;
using System.Collections.Generic;

namespace Softtek.Academy2018.Demo.Console
{
    public class ProjectWorkflow
    {
        private readonly ProjectManagementServiceClient _client;

        public ProjectWorkflow(ProjectManagementServiceClient client)
        {
            _client = client;
        }

        public void Create()
        {
            System.Console.WriteLine("---Create new Project---");

            System.Console.Write("Name:");
            string Name = System.Console.ReadLine();
            System.Console.Write("Area:");
            string Area = System.Console.ReadLine();
            System.Console.Write("TechnologyStack");
            string TechnologyStack = System.Console.ReadLine();

            CreateProjectRequest request = new CreateProjectRequest
            {
                Name = Name,
                Area = Area,
                TechnologyStack = TechnologyStack
            };

            CreateProjectResponse response = null;

            response = _client.CreateProject(request);

            if (response == null)
            {
                System.Console.WriteLine("Error: Unable to create the project");
            }
            else
            {
                if (!response.Success || response.ProjectId <= 0)
                {
                    System.Console.WriteLine($"Error: {response.Message}");
                }
                else
                {
                    System.Console.WriteLine($"Success: Project Created, ID: {response.ProjectId}");
                }
            }
            System.Console.WriteLine("------------------");
        }

        public void GetAll()
        {
            System.Console.WriteLine("Get all the projects...");
            GetAllProjectResponse response = _client.GetAllProject(new GetAllProjectRequest());

            if(response == null)
            {
                System.Console.WriteLine("Error: Response is null");
                System.Console.WriteLine("-------------");
                return;
            }

            //foreach (var p in response.Projects)
            //{
            //    System.Console.WriteLine($"Id {p.Id}\n" +
            //        $"Area {p.Area}\n" +
            //        $"Name {p.Name}\n" +
            //        $"Technology Stack {p.TechnologyStack}");
            //    System.Console.WriteLine("-------------");
            //}
        }
    }
}
