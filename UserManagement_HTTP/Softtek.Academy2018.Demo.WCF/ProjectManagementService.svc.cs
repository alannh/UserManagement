﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Softtek.Academy2018.Demo.WCF.Messages;
using Softtek.Academy2018.Demo.Business.Contracts;
using Softtek.Academy2018.Demo.Data.Contracts;
using Softtek.Academy2018.Demo.Data.Implementation;
using Softtek.Academy2018.Demo.Business.Implementation;
using Softtek.Academy2018.Demo.Domain.Model;

namespace Softtek.Academy2018.Demo.WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ProjectManagementService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ProjectManagementService.svc or ProjectManagementService.svc.cs at the Solution Explorer and start debugging.
    public class ProjectManagementService : IProjectManagementService
    {
        private readonly IProjectService _service;

        public ProjectManagementService()
        {
            IProjectRepository repository = new ProjectDataRepository();
            _service = new ProjectService(repository);
        }

        public CreateProjectResponse CreateProject(CreateProjectRequest request)
        {
            if (request == null)
            {
                return new CreateProjectResponse
                {
                    Success = false,
                    Message = "Error: request is null"
                };
            }

            Project project = new Project
            {
                Name = request.Name,
                Area = request.Area,
                TechnologyStack = request.TechnologyStack
            };

            int id = _service.Add(project);

            if (id == 0)
            {
                return new CreateProjectResponse
                {
                    Success = false,
                    Message = "Error: unable to create user"
                };
            }

            return new CreateProjectResponse
            {
                Success = true,
                ProjectId = id
            };
        }

        public GetAllProjectResponse GetAllProject(GetAllProjectRequest request)
        {
            if (request == null)
            {
                return new GetAllProjectResponse
                {
                    Success = false,
                    Message = "Request is null"
                };
            }

            ICollection<Project> projects = _service.GetAll();

            if (projects == null) return new GetAllProjectResponse
            {
                Success = false,
                Message = "Error: Unable to get projets"
            };

            ICollection<ProjectDTO> projectList = projects.Select(p => new ProjectDTO
            {
                Id = p.Id,
                Name = p.Name,
                Area = p.Area,
                TechnologyStack = p.TechnologyStack
            }).ToList();

            return new GetAllProjectResponse
            {
                Success = true,
                Projects = projectList
            };
        }
    }
}
