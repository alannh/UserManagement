﻿using Softtek.Academy2018.Demo.WCF.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Softtek.Academy2018.Demo.WCF
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IProjectManagementService" in both code and config file together.
	[ServiceContract]
	public interface IProjectManagementService
    {
        [OperationContract]
        CreateProjectResponse CreateProject(CreateProjectRequest request);

        [OperationContract]
        GetAllProjectResponse GetAllProject(GetAllProjectRequest request);
    }
}