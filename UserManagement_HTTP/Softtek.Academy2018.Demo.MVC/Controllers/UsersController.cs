﻿using Softtek.Academy2018.Demo.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Softtek.Academy2018.Demo.MVC.Controllers
{
    public class UsersController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            List<User> users = new List<User>();
            User user = new User { Id = 1, IS = "ALNH", FirstName = "Alan", LastName = "Navarro", ProjectId = 1, Project = new Project { Id = 1, Name = "Academia", TechnologyStack = ".NET"}, Salary = 1 };
            users.Add(user);
            user = new User { Id = 2, IS = "PEPI", FirstName = "Pepito", LastName = "Pepito", ProjectId = 1, Project = new Project { Id = 1, Name = "Academia", TechnologyStack = ".NET" }, Salary = 1 };
            users.Add(user);
            return View(users);
        }
    }
}