﻿using Newtonsoft.Json;
using Softtek.Academy2018.Demo.Domain.Model;
using Softtek.Academy2018.Demo.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Softtek.Academy2018.Demo.MVC.Controllers
{
    public class UsersController : Controller
    {
        // GET: User
        public async Task<ActionResult> Index()
        {
            //List<User> users = new List<User>();
            //User user = new User { Id = 1, IS = "ALNH", FirstName = "Alan", LastName = "Navarro", ProjectId = 1, Project = new Project { Id = 1, Name = "Academia", TechnologyStack = ".NET" }, Salary = 1 };
            //users.Add(user);
            //user = new User { Id = 2, IS = "PEPI", FirstName = "Pepito", LastName = "Pepito", ProjectId = 1, Project = new Project { Id = 1, Name = "Academia", TechnologyStack = ".NET" }, Salary = 1 };
            //users.Add(user);

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:2311/api/");
                //HTTP GET
                var response = await client.GetAsync("user");
                string content = await response.Content.ReadAsStringAsync();

                User user = JsonConvert.DeserializeObject<User>(content);

                UserViewModel viewModel = new UserViewModel
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    IS = user.IS
                };

                //var result = responseTask.Result;
                //if (result.IsSuccessStatusCode)
                //{

                //    var readTask = result.Content.ReadAsAsync<User[]>();
                //    readTask.Wait();

                //    var users = readTask.Result;
                //    return View(users);
                //}

                return View(viewModel);
            }
        }
    }
}

